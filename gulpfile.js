var gulp = require('gulp');
var rename = require('gulp-rename');
var sass = require('gulp-scss');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');

var dir = {
  script: {
    src: ['src/js/vendor/*.js', 'src/js/*.js'],
    dest: 'dist/',
    files: ['src/js/vendor/*.js', 'src/js/*.js']
  },
  style: {
    src: 'src/scss/init.scss',
    dest: 'dist/',
    files: ['src/scss/**/*.scss', 'src/scss/*.scss']
  },
  image: {
    src: ['src/imgs/*.jpg', 'src/imgs/*.png'],
    dest: 'dist/imgs/'
  }
}

gulp.task('css', function() {
  gulp.src(dir.style.src)
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(rename('app.css'))
    .pipe(gulp.dest(dir.style.dest))
    .pipe(browserSync.stream());
  // browserSync.reload()
})

gulp.task('js', function() {
  gulp.src(dir.script.src)
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(dir.script.dest));

  // gulp.src(dir.script.files)
  //   .pipe(concat('app.min.js'))
  //   .pipe(uglify())
  //   .pipe(gulp.dest(dir.script.dest));

  browserSync.reload()
})

gulp.task('img', function() {
  gulp.src(dir.image.src)
    .pipe(gulp.dest(dir.image.dest));
})

gulp.task('build', function() {
  gulp.src('dist')
    .pipe(gulp.dest('build'))

  gulp.src('dist/*.js')
    .pipe(gulp.dest('build/dist'))

  gulp.src('dist/*.css')
    .pipe(gulp.dest('build/dist'))

  gulp.src('./index.html')
    .pipe(gulp.dest('build'))

  gulp.src('dist/imgs/*.**')
    .pipe(gulp.dest('build/dist/imgs/'))
})

gulp.task('default', ['css', 'js'], function() {

  browserSync.init({
      server: {
        baseDir: "./"
      }
  });

  gulp.watch(dir.script.files, ['js', ]);
  gulp.watch(dir.style.files, ['css']);
  gulp.watch("*.html").on('change', browserSync.reload);

})

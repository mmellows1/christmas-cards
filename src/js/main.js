$(window).on('load', function() {

  function get_parameter_by_name(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  var family = {
    'default': {
      name: 'Wonderful person I do not know!',
      message: 'Hope you have a great christmas and new year!',
      img: 'bg-1'
    },
    'dad': {
      name: 'Chicken Dave',
      message: 'Missing you loads and really hope you have a wonderful christmas',
      img: 'bg-2'
    },
    'mum': {
      name: 'Lovely mummy!',
      message: 'I miss you guys so much, wish I could come back and spend christmas with you!',
      img: 'bg-3'
    },
    'bro': {
      name: 'Brother bohj',
      message: 'You little ape child, hope you have a wicked christmas you mexican dwarf',
      img: 'bg-4'
    },
    'nash': {
      name: 'the Nash\'s',
      message: 'Hope you have a wonderful christmas and I look forward to the wedding!',
      img: 'bg-5'
    },
    'platt': {
      name: 'the Platts',
      message: 'We wish you a lovely christmas from Korea',
      img: 'bg-6'
    }
  }

  var answers = {
    q1: {
      a: "1",
      a_txt: "Yakgwa",
      ua: null
    },
    q2: {
      a: "3",
      a_txt: "Soju",
      ua: null
    },
    q3: {
      a: "2",
      a_txt: "His paintings were too erotic",
      ua: null
    },
    q4: {
      a: "3",
      a_txt: "10",
      ua: null
    },
    q5: {
      a: "1",
      a_txt: "1945",  
      ua: null
    },
    q6: {
      a: "3",
      a_txt: "PyeongChang",  
      ua: null
    },
    q7: {
      a: "2",
      a_txt: "Food",  
      ua: null
    },
    q8: {
      a: "4",
      a_txt: "All of the above",  
      ua: null
    },
    q9: {
      a: "4",
      a_txt: "A bath house",
      ua: null
    },
    q10: {
      a: "4",
      a_txt: "木",
      ua: null
    }
  }

  var quiz;
  var completed = false;
  var $quiz_carousel = $('.quiz');
  var $quiz_carousel_next = $('.quiz').find('.next');
  var $quiz_carousel_active;
  var $quiz_carousel_active_next;

  var $card_wrapper = $('.cards');
  var $card = $('.card');
  var $card_controller = $card_wrapper.find('.controller');
  var $card_next = $card.find('.next');

  var score = 0;
  var $answers = $('#answers');

  function init_card() {
    // alert('working');
    var name = get_parameter_by_name('name');
    if(family[name]) {
      // on list
      do_write_card(name);
    } else {
      // off list
      do_write_card();
    }
  }

  // function do_remove_toggle_hints() {
  //   $('.quiz .hint').each(function() {
  //     $(this).hide();
  //   })
  // }

  function do_write_card(name) {
    if (!name) {
      name = 'default';
    } 

    $('#card-name').html(family[name].name);
    $('#card-message').html(family[name].message);
  }

  // init_card();
  
  function init_carousel() {
    quiz = $quiz_carousel.owlCarousel({
      mouseDrag: false,
      touchDrag: false,
      pullDrag: false,
      items:1,
      nav:false,
      dots: false
    })

    $quiz_carousel_next.on('click', function(index) {
      quiz.trigger('next.owl.carousel');
    })

    quiz.on('changed.owl.carousel', function(property) {
      var index = property.item.index + 1;
      var count = property.item.count;
      if(index == count && !completed) {
        completed = true;
        do_final_scores();
      } else {
        init_carousel_item('q' + index);  
      }
    })

    init_carousel_item('q1');
  }

  function init_carousel_item(id) {
    if($quiz_carousel_active !== $('#' + id)) {
      $quiz_carousel_active = $('#' + id);
      if($quiz_carousel_active.find('input')) { 
        $quiz_carousel_active.find('input').on('click', function() {
          set_answer(id, $(this).val());
          toggle_next_button();
        })
        do_question_animate();
      }
    }
  }

  function do_question_animate(id) {

    var title = $quiz_carousel_active.find('title');
    var txt = title.data('type');

    do_type_writer();
  }

  function toggle_next_button() {

    $quiz_carousel_active.find('.next').attr('disabled', false); 
  }

  function set_answer(id, answer) {

    answers[id].ua = answer;
  }

  function do_final_scores() {

    var index = 0;
    $.each(answers, function(key, value) {
      index = index + 1;
      if(value.a == value.ua) {
        score = score += 1;
        $answers.append(`
          <li class="text-green">
            You managed to get question ` + index + ` correct!
          </li>
        `);
      } else {
        $answers.append(`
          <li class="text-red">
            Oops! You got question ` + index + ` incorrect!<br />
            The answer was "<i>` + answers[key].a_txt + `</i>"
          </li>
        `);
      }
    })

    do_count_up('total-score', score);
  }

  function do_count_up(ele, score) {

    var options = {
      useEasing: true, 
      useGrouping: true, 
      separator: ',', 
      decimal: '.', 
      prefix: 'You got <b class="text-red">', 
      suffix: '</b> out of 10'
    };

    var count_up = new CountUp(document.getElementById(ele), 0, score, 0, 2.5, options);

    setTimeout(function() {
      count_up.start();
      do_show_answers();
    }, 300)
    
  }

  function do_show_answers() {
    $answers.addClass('fade-in');
  }

  function do_show_options() {
    $quiz_carousel_active.find('.option').each(function(index) {
      var $this = $(this);
      setTimeout(function() {
        $this.addClass('fade-in');
      }, index * 300);
    })
  }

  function do_type_writer() {

    var array = [];
    var str = $quiz_carousel_active.find('.title').data('type');
    var id = '#' + $quiz_carousel_active.find('.title').attr('id');

    array.push(str);

    new Typed(id, {
      strings: array,
      startDelay: 0,
      typeSpeed: 50,
      onComplete: function() {
        do_show_options();
      }
    })
  }

  $card.click(function() {
    $(this).addClass('active');
    init_card();
  })

  $card_next.on('click', function() {

    init_carousel();
  })

  function init() {
    var name = get_parameter_by_name('name');
    
    if (!name) {
      name = 'default';
    } 

    $('.card').find('.front-face').addClass(family[name].img);
  }

  init();
})